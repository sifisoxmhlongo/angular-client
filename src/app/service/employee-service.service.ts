import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from '../model/employee';

@Injectable()
export class EmployeeService {

  private employeeUrl: string = "";

  constructor(private http: HttpClient) { 
    this.employeeUrl = 'http://localhost:8080/employees';
  }

  public findAll(): Observable<Employee[]>{
    return this.http.get<Employee[]>(this.employeeUrl);
  }

  public save(emp: Employee){
    return this.http.post<Employee>(this.employeeUrl, emp);
  }
}
