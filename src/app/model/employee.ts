export class Employee {

    id!: any;
    name!: string;
    surname!: string;
    email!: string;
    role!: string;
    salary!: number;
}
